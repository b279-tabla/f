/* import './App.css'; */
import AppNavbar from './components/AppNavbar';

import Home from './pages/Home';
import Services from './pages/Services';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from "./pages/Error";
import Dashboard from './pages/Dashboard';
import AllServices from './pages/AllServices';
import ServiceView from './components/ServiceView';
import ServiceAppointment from './components/ServiceAppointment';


import { Container } from "react-bootstrap";
import { Route, Routes } from 'react-router-dom';
import { BrowserRouter as Router} from "react-router-dom";
import { useState, useEffect } from "react";
import { UserProvider } from "./UserContext";



function App() {

 // Creating a user state for global scope

  /*const [user, setUser] = useState({
    email: localStorage.getItem("email")
  })*/

  const [user, setUser] = useState({
    id: localStorage.getItem("id"),
    isAdmin: localStorage.getItem("isAdmin"),
    email: localStorage.getItem("email"),
    token: localStorage.getItem("token")
})

// Function for clearing the storage on logout
const unsetUser = () => {
  localStorage.clear();
}

useEffect(() => {
  console.log(user);
  console.log(localStorage);
}, [user])

useEffect(()=>{

  fetch(`http://localhost:5000/users/details`, {
    method: "POST",
    headers:{
      Authorization: `Bearer ${localStorage.getItem("token")}`
    }
  })
  .then(res => res.json())
  .then(data => {
    console.log(data);

    if(data._id !== undefined){
      console.log(" makuha");
      setUser({

          id: localStorage.setItem("id", data._id),
          isAdmin: localStorage.setItem("isAdmin", data.isAdmin),
          email: localStorage.setItem("email", data.email) 

      });
    }
    else{
      console.log("di makuha");
      setUser({
        
        id: null,
        isAdmin: null
      });
    }
    
  })

}, [])



  return (
    <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
      <AppNavbar/>
        <Container fluid>
          <Routes>
            <Route path="/" element={<Home/>}/>
            <Route path='/Services' element={<Services/>}/>
            <Route path='/serviceAppointment/:serviceId' element={<ServiceAppointment/>}/>
            <Route path='/servicesView/:serviceId' element={<ServiceView/>} />
            <Route path='/Register' element={<Register/>}/>
            <Route path='/Login' element={<Login/>}/>
            <Route path='/Logout' element={<Logout/>}/>
            <Route path="*" element={<Error/>}/>
            <Route path="/Dashboard" element={<Dashboard />} />
            <Route path="/AllServices" element={<AllServices/>} />
          </Routes>
        </Container>
    </Router>
    </UserProvider>
  )
};

export default App;
