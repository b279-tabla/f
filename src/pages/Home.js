import LandingPage from "../components/LandingPage";

export default function Home(){

    const data = {
        title: "Your Amazing pets are Important to us",
        content: "We are on duty 24 hours a day for the health of your beautiful pets",
        destination: "/register",
        label: "Get Started"
    }

    return(
        <>
            <LandingPage landingProps={data}/>
        </>
    )



}