import { useContext } from "react"
import UserContext from "../UserContext";
import {Container, Col, Row} from "react-bootstrap"
import { Navigate } from "react-router-dom";
import LandingPage from "../components/LandingPage";


export default function Dashboard(){
     const { user } = useContext(UserContext);
    console.log(user);

    const data = {
     title: "WELCOME TO YOUR DASHBOARD!",
     content: "Here is where you can control things. Let's work together!",
     destination: "/AllServices",
     label: "See All Services",

     }

   

	return(
          <>
        {
        (user.isAdmin) ?
        <Container fluid className="">
        <Row className="">
        <Col className="" xs={12} md={2} lg={2}>
        </Col>
        <Col className="" xs={12} md={10} lg={10}>
        <LandingPage landingProps={data}/>
        </Col>
        
        </Row>
     </Container>
        :
        <Navigate to={"/"}/>
        }
      </>

	)
}
