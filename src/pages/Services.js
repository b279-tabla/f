import { Container} from "react-bootstrap";
import ServicesCard from "../components/ServicesCard";
import { useEffect, useState, useContext } from "react";
import UserContext from "../UserContext";
import { Navigate } from "react-router-dom";

export default function Services(){

    const { user } = useContext(UserContext);

    const [services, setServices] = useState([]);

    useEffect(() => {
        

        /* For the User View All the Available Services will be shown in the interface, if isAvaliable is False. It will be hidden. */


		fetch(`http://localhost:5000/vet/Available`)
		.then(res => res.json())
		.then(data => {
			console.log(data);


			setServices(data.map(services => {
				return (
					<ServicesCard key={services._id} serviceProp={services}/>
					)
			}))

		})
	}, [])

    return( 
        (user.isAdmin)
		?
			<Navigate to="/dashboard" />
		:	
	
        <Container fluid className="p-5 ServicesLandingPage">
        <h1 className="ServicesH1 text-center mt-5 pt-5">What we Offer?</h1>
        <p className="ServicesP text-center mb-5">We offer a wide range of services which includes following below</p>
        {services}
        
        </Container>
    )
}