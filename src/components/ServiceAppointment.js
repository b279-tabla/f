import {Container, Card, Button, Row, Col, Modal, Form} from "react-bootstrap";
import { useParams } from "react-router-dom";
import {useState, useEffect, useContext} from "react";
import UserContext from "../UserContext";
import Swal from "sweetalert2";
import {Link} from "react-router-dom";
import { Navigate } from "react-router-dom";

export default function ServiceAppointment(){

    const { user } = useContext(UserContext);

	// module that allows us to retrieve the courseId passed via URL
	const { serviceId } = useParams();

    const [name, setName] = useState("");
	const [procedure, setProcedure] = useState("");
	const [price, setPrice] = useState(0);
    const [quantity, setQuantity] = useState(0);

    const [isActive, setIsActive] = useState(false);

	// Start for : retrieving data for Service Id
	function serviceData(e){
		e.preventDefault();
	}
		fetch (`http://localhost:5000/vet/${serviceId}`)
        

		.then(res => res.json())
		.then(data => {

			setName(data.name);
			setProcedure(data.procedure);
			setPrice(data.price);
		})

	// End for : retrieving data for Service Id


    const makeAppointment = (e) => {
        e.preventDefault();
    
        fetch(`http://localhost:5000/consult/${serviceId}/addToConsultation`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                quantity: quantity
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
    
            if (data) {
                Swal.fire({
                    title: "APPOINTMENT ADDED SUCCESSFULLY!",
                    icon: "success",
                    text: `"The new service was added.`,
                });
    
            }
            else {
                Swal.fire({
                    title: "ADD PRODUCT UNSSUCCESSFUL!",
                    icon: "error",
                    text: `The system is experiencing trouble at the moment. Please try again later.`,
                });
            }
    
        })
        setQuantity(0);
    }



    useEffect(() => {

		if (quantity > 0) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	
	}, [quantity]);


    return(
        (user.token !== null) ?
        <> 
            <div className="p-5 mt-5">
            <h1>Make an Appointment!</h1>
            </div>

            <Row className="">
			<Col lg={{ span: 6, offset: 3 }}>
			    <Card>
				{/* After retriving all Services Details, the Data (Name, Procedure, Price, will be place here) */}
					<Card.Body className="p-5" onSubmit={e => serviceData(e)}>
						<Card.Title className="mb-3">{name}</Card.Title>
						<Card.Subtitle className="mb-3">Description:</Card.Subtitle>
						<Card.Text className="mb-3">{procedure}</Card.Text>
						<Card.Subtitle className="mb-3">Price:</Card.Subtitle>
						<Card.Text className="mb-3">PhP {price}</Card.Text>
						<Card.Subtitle className="mb-3">How many Appointment would you need:</Card.Subtitle>
                        
                        <Form className='' onSubmit={e => makeAppointment(e)}>
                    
                            {/* Name */}
                        <Form.Group className="mb-3" controlId="FirstName">
                        <Form.Label>1 Appointment per Pet</Form.Label>
                        <Form.Control type="number" value={quantity} onChange={e => setQuantity(e.target.value)} placeholder="Enter Quantity" required/>
                        </Form.Group>

                        {/* Submit Button */}

                        {isActive ?
                        <Button variant="primary" type="submit" id="submitBtn">Appointment!</Button>
                        :
                        <Button variant="danger" type="submit" id="submitBtn" disabled>Appointment!</Button>
                        }
                        </Form>

				    </Card.Body>		
				</Card>
	        </Col>
		</Row>
        </>

        :

        <Navigate to="/Login"/>

    )
}
