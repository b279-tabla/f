const servicesData = [
    {
        id: "wdc001",
        name: "Radiology Assistant",
        description: "Radiology is the medical discipline that usees radiology to diagnose patients.",
        price: 55000,
        onOffer: true
    },
    {
        id: "wdc002",
        name: "Ambulance Service",
        description: "We are just a phone call away. we have standby ambulances to help solve emmergencies ",
        price: 1500,
        onOffer: true
    },
    {
        id: "wdc003",
        name: "Dental Care Service",
        description: "All animals should have healthy teeths , so we ensure to provide care for all animals",
        price: 700,
        onOffer: true
    },
    {
        id: "wdc004",
        name: "Surgical Operation",
        description: "We provide surgical operations to all animals that are in need of it 24/7",
        price: 70000,
        onOffer: true
    },
    {
        id: "wdc005",
        name: "Partution service",
        description: "Our maternity service provides services with our private and hygenic delivery",
        price: 30000,
        onOffer: true
    },
    {
        id: "wdc005",
        name: "Pet Hotel service",
        description: "Our hotel service works for you when you're on vacation or at work",
        price: 500,
        onOffer: true
    },


]

export default servicesData;